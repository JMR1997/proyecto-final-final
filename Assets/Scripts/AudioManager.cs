﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class AudioManager : MonoBehaviour
{
    public Sonido[] sonidos;

    // Start is called before the first frame update
    void Awake()
    {
        foreach(Sonido s in sonidos)
        {
            s.source = gameObject.AddComponent<AudioSource>();
            s.source.clip = s.clip;

            s.source.volume = s.volumen;
            s.source.pitch = s.pitch;
            s.source.loop = s.loop;
        }
    }

    private Sonido ObtenerSonido(string nombre)
    {
        Sonido s = Array.Find(sonidos, sonido => sonido.nombre == nombre);
        return s;
    }
    public void Play(string nombre)
    {
        Sonido s = ObtenerSonido(nombre);
        s.source.Play();
    }

    public void Stop(string nombre)
    {
        Sonido s = ObtenerSonido(nombre);
        s.source.Stop();
    }

    //Para usar
    //FindObjectOfType<AudioManager>.Play("nombreSonido");
    
}
