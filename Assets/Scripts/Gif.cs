﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gif : MonoBehaviour
{
    public Texture2D[] frames;
    public int fps = 10;
    public Texture2D imagenPorDefecto;
    private bool esReproducir { get; set; }

// Update is called once per frame
void Update()
    {
        if (Application.platform != RuntimePlatform.Android)
        {
            if (esReproducir)
            {
                ReproducirGif();
            }
        }
        #region Android
        else
        {
            ReproducirGif();
        }
        #endregion
    }

    public void OnHover()
    {
        esReproducir = true;
    }
    public void OnExitHover()
    {
        esReproducir = false;
        ImagenPorDefecto();
    }
    private void ReproducirGif()
    {
        int index = (int)(Time.time * fps) % frames.Length;
        GetComponent<RawImage>().texture = frames[index];
    }

    private void ImagenPorDefecto()
    { 
        GetComponent<RawImage>().texture = imagenPorDefecto;
    }
}
