﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MenuPpalManager : MonoBehaviour
{
    private TransitionManager tm;
    // Start is called before the first frame update
    void Start()
    {
        tm = FindObjectOfType<TransitionManager>();
    }

    // Update is called once per frame
    void Update()
    {
        _FuncionesComunes.ComprobarCerrarAplicacion();
        
    }

    //Llama a la transicion y cuando termina la transición carga la escena.
    //¿Se podria hacer que cargue de mientras? Si pero la animacion se veria a trompicones da igual el dispositivo
    public void cargarEscena(string nombreEscena)
    {
        //Pasa un numero no valido para que sea random
        Animator anim = tm.empiezaAnim(-1);
        StartCoroutine(esperarFinAnimToLoad(nombreEscena, anim));
    }

    public IEnumerator esperarFinAnimToLoad(string nombreEscena,Animator anim)
    {
        AnimatorStateInfo estadoAnim = anim.GetCurrentAnimatorStateInfo(0);
        while(!estadoAnim.IsName("parado"))
        {
            estadoAnim = anim.GetCurrentAnimatorStateInfo(0);
            yield return null;
        }
        SceneManager.LoadScene(nombreEscena);
    }
    public void Btn_Salir()
    {
        Application.Quit();
    }

}
