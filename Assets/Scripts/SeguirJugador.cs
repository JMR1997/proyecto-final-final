﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SeguirJugador : MonoBehaviour {

    public Transform target;

    public float smoothTime = 0.125f;
    public float offsetY = 3;

    public bool seguirX = true;
    public bool seguirY = true;

    private Vector2 velocity;

	// Use this for initialization
	void Start () {
		if(target == null)
        {
            target = GameObject.FindGameObjectWithTag("Player").transform.transform;
        }
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        //Posicion actual,objetivo,,tiempo a tardar
        float posX = Mathf.SmoothDamp(transform.position.x,target.position.x,ref velocity.x, smoothTime);
        float posY = Mathf.SmoothDamp(transform.position.y, target.position.y + offsetY, ref velocity.y, smoothTime);

        //Modifica el eje X si es necesario
        if (seguirX)
        {
            transform.position = new Vector3(
            posX,
            transform.position.y,
            transform.position.z
            );
        }

        //Modifica el eje Y si es necesario
        //si ya se mopdifico el eje X se respeta la moodificacion y sino mantiene la posicion que ya tenia
        if (seguirY)
        {
            transform.position = new Vector3(
            transform.position.x,
            posY,
            transform.position.z
            );
        }
    }
}

