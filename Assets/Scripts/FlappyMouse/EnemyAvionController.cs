﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAvionController : MonoBehaviour
{
    #region variables

    private Vector2 posicion;
    public float z=0;
    private bool esActivadoLimite=false;

    #endregion


    void Start()
    {
        posicion = transform.position;
        transform.Rotate(new Vector3(0,0,z));
    }

    void Update()
    {
        if (z == Constantes.POSICIONESZAVION[0])
        {
            posicion.x = posicion.x - Constantes.VELOCIDAD * Time.deltaTime;
            posicion.y = posicion.y - Constantes.VELOCIDAD * Time.deltaTime;
        }
        else
        {
            posicion.x = posicion.x - Constantes.VELOCIDAD* Time.deltaTime;
            posicion.y = posicion.y + Constantes.VELOCIDAD * Time.deltaTime;
        }
      
        transform.position = posicion;
    }

    /*
     * Metodo que comprueba la colision con otro elementos que posean un Collider2D.
     * En este caso, al detectar colision con el destroyer o limit, el objeto asociado a este script se destruye.
     */

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals("destroyer") || collision.gameObject.tag.Equals("Limit"))
        {
            if (esActivadoLimite)
            {
                Destroy(gameObject);
            }
            else
            {
                esActivadoLimite = true;
            }
           
        }
    }

}
