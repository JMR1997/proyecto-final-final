﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoGeneratorFMController : MonoBehaviour
{
    #region variables

    /********************************************************************
     *PREFABS
     ********************************************************************/
    public GameObject disquetePrefab;
    public GameObject avionPrefab;
    public GameObject columnPrefab;

    private float generatorTimer=3f;
   

    #endregion

    //Función crea los enemigos disquetes con posicion de y aleatoria
    private void CrearEnemigoDisquete()
    {
        int indiceAlAzar = Random.Range(1,7);
        int indiceAlAzarAnterior;

        Vector2 posicion = new Vector2(transform.position.x,Constantes.POSICIONESPOSIBLESENEMIGOSDISQUETES[indiceAlAzar]);
        Instantiate(disquetePrefab, posicion, Quaternion.identity);

        indiceAlAzarAnterior = indiceAlAzar;
        while(indiceAlAzar==indiceAlAzarAnterior)
            indiceAlAzar = Random.Range(1,7);

        posicion.y = Constantes.POSICIONESPOSIBLESENEMIGOSDISQUETES[indiceAlAzar];
        Instantiate(disquetePrefab, posicion, Quaternion.identity);

    }

    //Intancia al enemigo avion y segun su z la y se posiciona arriba del canvas o abajo
    private void CrearEnemigoAvion()
    {
        int indicieAleatorio =Random.Range(0,2);
        float z = Constantes.POSICIONESZAVION[indicieAleatorio];
        float y;

         if (z == Constantes.POSICIONESZAVION[0])
         {
             y = 8f;
         }
         else
         {
             y = -10f;
         }

        avionPrefab.GetComponent<EnemyAvionController>().z = z;
        Vector2 posicion = new Vector2(transform.position.x+3f,y);
        Instantiate(avionPrefab, posicion, Quaternion.identity);
    }

    //Instancia a las columnas en una posición y aleatoria
    private void CrearColumnas()
    {
        float yMinimo = -4.21f;
        float yMaximo = 1.37f;
        Vector2 posicion = new Vector2(8f, Random.Range(yMinimo, yMaximo));
        Instantiate(columnPrefab, posicion, Quaternion.identity);
        //ymin=-3.59 y max=3.57 x=8f;
    }

    //Genera enemigos de forma continua
    public void GenerarEnemigos(Constantes.ModoDeJuego modoDeJuego)
    {

        
        if (modoDeJuego== Constantes.ModoDeJuego.FlappyMouseOriginal)
        {
            InvokeRepeating("CrearColumnas", 0f, generatorTimer);
        }
        else
        {
            InvokeRepeating("CrearEnemigoDisquete", 0f, generatorTimer);
            InvokeRepeating("CrearEnemigoAvion", 10f, generatorTimer + 10);
        }
       
    }
    //Cancela la invocación de enemigos y los elimina de la pantalla
    public void PararGenerador(Constantes.ModoDeJuego modoDeJuego)
    {
        
        if (modoDeJuego==Constantes.ModoDeJuego.FlappyMouseOriginal)
        {
            CancelInvoke("CrearColumnas");
        }
        else
        {
            CancelInvoke("CrearEnemigoDisquete");
            CancelInvoke("CrearEnemigoAvion");
        }
       
        Object[] allEnemies = GameObject.FindGameObjectsWithTag("Enemigo");
       
        foreach(GameObject enemigo in allEnemies)
        {
            Destroy(enemigo);
        }
        
    }
}
