﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColumnasEnemyController : MonoBehaviour
{
    #region variables

    private Rigidbody2D rb2d;

    #endregion
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = Vector2.left * Constantes.VELOCIDAD;
    }

    /*
     * Metodo que comprueba la colision con otro elementos que posean un Collider2D.
     * En este caso, al detectar colision con el destroyer, el objeto asociado a este script se destruye.
     */


    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "destroyer")
        {
            Destroy(gameObject);
     
        }
    }
    
}
