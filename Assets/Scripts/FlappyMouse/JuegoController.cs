﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class JuegoController : MonoBehaviour
{
    #region Declaración de variables
    /************************************************************
     * Elementos de la interfaz gráfica                         *
     ************************************************************/
    public GameObject jugador;
    public GameObject titulo;
    public GameObject fondoObject;
    public GameObject fondoOriginalGameObject;
    public GameObject fondoRevolutionGameObject;

    public RawImage fondoOriginal;
    public RawImage fondoRevolution;

    public GameObject generadorDeEnemigos;
    public GameObject uiPuntuacion;
    public Text puntos;
    public GameObject uiGameOver;

    private Constantes.ModoDeJuego modoDeJuego;

    //Control de puntuaje
    private int puntosTotales =0;
    private string claveScore = "Max Score";

    private float velocidadParallax=0.02f;
    
    public enum EstadoDelJuego { Parado, Jugando, Finalizado, PreparadoParaReinicio };
  
    public EstadoDelJuego estado = EstadoDelJuego.Parado;
    private bool haSidoPulsadoJugar = false;

    //Musica de fondo
    private AudioManager audioMananger;
    public string musicaFondo;

    #endregion

    void Start()
    {
        audioMananger = FindObjectOfType<AudioManager>();
    }

    void Update()
    {
        
        ActualizarJuego();
    }

    /*
     * Metodo usado por el boton de jugar. Pone a verdadero el booleano ha sidoPulsadoJugar y se la pasa como parametro que modo de juego queremos
     * @param string
     */
    public void PlayButton(int modoDeJuego)
    {
        haSidoPulsadoJugar = true;
        this.modoDeJuego = (Constantes.ModoDeJuego)modoDeJuego;
        claveScore += this.modoDeJuego==Constantes.ModoDeJuego.FlappyMouseOriginal?" FMOriginal":" FMRevolutions";
    }
    //Metodo llamado por el boton cancelar
    public void CancelButton()
    {
        _FuncionesComunes.CargarEscena("Menu");
    }

    //Comprueba el estado actual del juego y ejecuta las instrucciones que sean convenientes
    private void ActualizarJuego()
    {
        _FuncionesComunes.ComprobarCerrarAplicacion();
        if (EstadoDelJuego.Parado == estado && haSidoPulsadoJugar)
        {
            estado = EstadoDelJuego.Jugando;
            titulo.SetActive(false);
            jugador.SetActive(true);
            uiPuntuacion.SetActive(true);
            fondoObject.SetActive(false);
            if (modoDeJuego==Constantes.ModoDeJuego.FlappyMouseOriginal)
            {
                fondoOriginalGameObject.SetActive(true);
            }
            else
            {
                fondoRevolutionGameObject.SetActive(true);
                InvokeRepeating("IncrementarPuntuacion", 0f, 2f);
            }
            generadorDeEnemigos.SendMessage("GenerarEnemigos", modoDeJuego);
            audioMananger.Play(musicaFondo);
        }
        else if (EstadoDelJuego.Jugando == estado)
        {
            EfectoParallax();
        }else if (EstadoDelJuego.PreparadoParaReinicio == estado)
        {
            PararJuego();
        }
    }

    //Crea el efecto parallax para el fondo del juego
    private void EfectoParallax()
    {
        float velocidadFinal = velocidadParallax * Time.deltaTime;
        RawImage fondoAMover;
        if (modoDeJuego==Constantes.ModoDeJuego.FlappyMouseOriginal)
        {
            fondoAMover = fondoOriginal;
        }
        else
        {
            fondoAMover = fondoRevolution;
        }
        fondoAMover.uvRect = new Rect(fondoAMover.uvRect.x + velocidadFinal, fondoAMover.uvRect.y, fondoAMover.uvRect.width, fondoAMover.uvRect.height);
    }
    //Metodo que al ser llamado incrementa la punctuación del juego y si esta es mayor que GetMaxScore(), la guarda
    public void IncrementarPuntuacion()
    {
       puntos.text = (++puntosTotales).ToString();
        if (puntosTotales >_FuncionesComunes.GetMaxScore(claveScore))
        {
            _FuncionesComunes.SaveScore(claveScore, puntosTotales);
        }
    }
    //Metodo que detiene el juego y activa y/o desactiva los elementos gráficos
    private void PararJuego()
    {
        generadorDeEnemigos.SendMessage("PararGenerador",modoDeJuego);
        jugador.SendMessage("QuitarJugador");

        if (modoDeJuego== Constantes.ModoDeJuego.FlappyMouseRevolution)
        {
            CancelInvoke("IncrementarPuntuacion");
            fondoRevolutionGameObject.SetActive(false);
        }
        else
        {
            fondoOriginalGameObject.SetActive(false);
        }

        puntos.text = "Max Score: " + _FuncionesComunes.GetMaxScore(claveScore);
        uiGameOver.SetActive(true);
        estado = EstadoDelJuego.Finalizado;
        fondoObject.SetActive(true);      
    }
    //Metodo que reinicia el juego
    public void ReiniciarJuego()
    {
        _FuncionesComunes.CargarEscena("FlappyMouse");
    }

}
