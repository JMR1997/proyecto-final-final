﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PersonajeController : MonoBehaviour
{
    #region Declaración de variables
    private Rigidbody2D rb2d;
    private float upForce = 100f;
    public GameObject juego;

    private AudioManager audioManager;
    public string nSonidoSaltar = "Jump";
    public string nSonidoDmg = "Damage";
    #endregion

    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        audioManager = FindObjectOfType<AudioManager>();
    }

    // Update is called once per frame
    void Update()
    {
        Saltar();
    }

    //Metodo que comprueba el boton pulsado y hace que la y del personaje se incremente
    private void Saltar()
    {
        if (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Space))
        {
            rb2d.velocity = Vector2.zero;// Detiene al personaje para que luego funcione el impulso
            rb2d.AddForce(Vector2.up * upForce);
            audioManager.Play(nSonidoSaltar);
        }
    }
    //Metodo que comprueba las colisiones del personaje con otros elemenos con Collider2D
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Limit" || collision.gameObject.tag=="Enemigo")
        {
            audioManager.Play(nSonidoDmg);
            juego.GetComponent<JuegoController>().estado = JuegoController.EstadoDelJuego.PreparadoParaReinicio;
        }

        else if (collision.gameObject.tag == "Puntos")
        {
            juego.SendMessage("IncrementarPuntuacion");
        }
    }
    
    //Metodo que limpia al jugador de la pantalla

    public void QuitarJugador()
    {
        Object player = GameObject.FindGameObjectWithTag("Player");
        Destroy(player);
    }



}
