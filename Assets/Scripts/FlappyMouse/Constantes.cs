﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

    public class Constantes
    {
        public enum ModoDeJuego { FlappyMouseOriginal, FlappyMouseRevolution };
        public const float VELOCIDAD = 2f;

        /*********************************************************************************************
       *                                       MEDIDAS JUEGO
       * *******************************************************************************************
       *  Arriba=2.25f
       *  1er Centro Arriba=1f
       *  2º Centro Arriba=-0.25f
       *  Centro=1.51f
       *  1er Centro Abajo= -3f
       *  2º Centro Abajo= -4.25f
       *  Abajo = -5.5f
       *********************************************************************************************/

        public static readonly float[] POSICIONESPOSIBLESENEMIGOSDISQUETES = { 2.25f, 1f, -0.25f, -1.51f, -3f, -4.25f, -5.5f };
        public static readonly float[] POSICIONESZAVION = { 18f, -18f };
    }


