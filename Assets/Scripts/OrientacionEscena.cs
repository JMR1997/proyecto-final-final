﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Orientacion
{
    Variable = 0,
    SoloVertical = 1,
    SoloHorizontal = 2,
}


public class OrientacionEscena : MonoBehaviour
{
    public Orientacion or;
    // Start is called before the first frame update
    void Start()
    {
        if(or == Orientacion.SoloVertical) Screen.orientation = ScreenOrientation.Portrait;
        if (or == Orientacion.SoloHorizontal) Screen.orientation = ScreenOrientation.Landscape;
        //else no se modifica y se permite el cambio
    }

}
