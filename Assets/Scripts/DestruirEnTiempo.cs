﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestruirEnTiempo : MonoBehaviour
{

    public float tiempoParaDestruir = 1f;
    private float tiempoRes;

    private void Start()
    {
        tiempoRes = tiempoParaDestruir;
    }
    // Update is called once per frame
    void Update()
    {
        if(tiempoRes <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            tiempoRes -= Time.deltaTime;
        }
    }
}
