﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TransitionManager : MonoBehaviour
{
    private static TransitionManager _instance;

    public static TransitionManager Instance { get { return _instance; } }
    private List<GameObject> listaTransiciones;
    public GameObject transicionEmpezada;

    private void Awake()
    {

        if (_instance != null && _instance != this)
        {
            if (transicionEmpezada != null) //Viene de otra escena que haga sus cosas y se destruya
            {
                //Este instancia de transition manager viene de otra escena y se ejecuta la animacion de salida antes de destruirse
                Animator anim = transicionEmpezada.GetComponent<Animator>();//.Play("salida");
                anim.Play("salida");
                StartCoroutine(tranSalidaDestruir(anim));
            }
            else
            {
                Destroy(this.gameObject);
            }
        }
        else
        {
            _instance = this;
        }

        //Carga la lista de animaciones
        listaTransiciones = new List<GameObject>();
        for (int i = 0; i < transform.childCount ; i++)
        {
            listaTransiciones.Add(transform.GetChild(i).gameObject);
        }

        DontDestroyOnLoad(this);
    }

    private IEnumerator tranSalidaDestruir(Animator anim)
    {
        yield return new WaitForSeconds(0.1f);
        anim.Play("salida");
        AnimatorStateInfo estadoAnim = anim.GetCurrentAnimatorStateInfo(0);
        while (!estadoAnim.IsName("parado"))
        {
            estadoAnim = anim.GetCurrentAnimatorStateInfo(0);
            yield return null;
        }
        Destroy(this.gameObject);
    }

    public Animator empiezaAnim(int indice)
    {
        if (indice >= listaTransiciones.Count || indice < 0)
        {
            indice = UnityEngine.Random.Range(0, (listaTransiciones.Count - 1));
        }
        Animator anim = listaTransiciones[indice].GetComponent<Animator>();
        anim.gameObject.SetActive(true);
        anim.Play("entrada");
        transicionEmpezada = listaTransiciones[indice];
        return anim;
    }

    private void OnLevelWasLoaded(int level)
    {
        if (transicionEmpezada != null) { transicionEmpezada.GetComponent<Animator>().Play("salida"); }
    }
}
