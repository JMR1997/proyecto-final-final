﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moverHaciaAbajo : MonoBehaviour
{
    public float velocidad = 0.05f;

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector2.down * velocidad * Time.deltaTime);
    }
}
