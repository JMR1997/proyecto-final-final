﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemigoDispara : MonoBehaviour
{
    public GameObject disparo;
    public Transform posicionDisparo;

    public float tiempoEntreAtaques;
    private float tiempoResAtacar;
    public float tiempoMinAleatorio = 0.5f;
    public float tiempoMaxAleatorio = 2f;

    public bool avanzaHaciaAbajo = true;
    public float velocidadAvance = 0.25f;

    public bool compruebaSiTieneEnemigoDelante = true;
    public float distanciaComprobar = 3f;


    private Vector2 posicionObjetivo;
    //Es una constante, pero Vector2 no permite ser constante
    private Vector2 posicionNull = new Vector2(9999, 9999);
    // Start is called before the first frame update
    void Start()
    {
        tiempoResAtacar = Random.Range(tiempoMinAleatorio,tiempoMaxAleatorio);
        Vector2 posActual = transform.position;

        //Porque vector2 o puede ser null usamos este valor como valor nulo, no válido
        posicionObjetivo = posicionNull;
    }

    // Update is called once per frame
    void Update()
    {
        if (avanzaHaciaAbajo)
        {
            //Se mueve hacia abajo si este enemigo lo hace
            transform.Translate(Vector2.down * velocidadAvance * Time.deltaTime);
            //Actualiza la posicion de referencia para mirar si hay enemigos
            Vector2 posActual = transform.position;
        }

        if (tiempoResAtacar > 0)
        {
            tiempoResAtacar -= Time.deltaTime;
        }
        else
        {
            if (compruebaSiTieneEnemigoDelante)
            {
                //Comprueba por si tiene un enemigo delante para no disparar
                RaycastHit2D hit = Physics2D.Raycast(posicionDisparo.position, Vector2.down,distanciaComprobar);
                if(hit.collider != null)
                {
                    if(hit.collider.gameObject == gameObject)
                    {
                        //No hay otro enemigo, es el mismo. Asi que dispara
                        ataca();
                    }
                }
                else
                {
                    //No hay nada, ataca
                    ataca();
                }
            }
            else
            {
                //Ataca sin comprobar
                ataca();
            }
            
        }
    }

    private void ataca()
    {
        Instantiate(disparo, posicionDisparo.position, Quaternion.identity);

        //tiempoResAtacar = tiempoEntreAtaques;
        tiempoResAtacar = Random.Range(tiempoEntreAtaques+tiempoMinAleatorio, tiempoEntreAtaques+tiempoMaxAleatorio);
    }

}
