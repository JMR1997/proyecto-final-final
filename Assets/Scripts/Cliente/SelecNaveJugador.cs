﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine.UI;
using UnityEngine;
using System;

public class SelecNaveJugador : MonoBehaviour
{
    [Header("Prefab jugador generico")]
    public GameObject prefabNave;
    public GameObject mapaJuego;


    //Posicion del array
    private int naveActual = 0;
    //Posibles naves a elegir
    [Header("Naves disponibles")]
    public JugadorClienteScriptable[] listaNaves;
    //Sus correspondientes splash arts
    [Header("Splash Arts naves")]
    public Sprite[] listaSplashArts;


    //Controles del HUD
    [Header("Elementos de UI")]
    public TextMeshProUGUI textoNombre;
    //Para modificar el recorrido en funcion de los valores
    public Slider sliderVida;
    public Slider sliderAtaque;
    public Slider sliderVelocidad;
    //Para modificar el color en funcion de sus valores
    public Image rellenoSliderVida;
    public Image rellenoSliderAtaque;
    public Image rellenoSliderVelocidad;
    //Splash Art
    public Image fondoSplashArt;

    //Musica
    private AudioManager audioManager;
    private string musicaDeFondo = "BgMusic";

    private void Start()
    {
        cambiarNave(0);
        audioManager = FindObjectOfType<AudioManager>();
    }

    public void siguienteNave()
    {
        cambiarNave(1);
    }

    public void naveAnterior()
    {
        cambiarNave(-1);
    }

    private void cambiarNave(int desplazamiento)
    {
        //Cambia la posicion
        int nuevaPos = naveActual + desplazamiento;
        //Si se ha salido por encima o por debajo del array corrige el valor a uno válido
        if (nuevaPos < 0) nuevaPos = listaNaves.Length - 1;
        if (nuevaPos >= listaNaves.Length) nuevaPos = 0;

        //Asigna el valor a la variable global
        naveActual = nuevaPos;
        //Y realiza los cambios de sprites,nombre y caracteristicas de la interfaz
        
        //Cambia el nombre
        textoNombre.text = listaNaves[naveActual].nombre;

        //Cambia la imagen / splash art
        fondoSplashArt.sprite = listaSplashArts[naveActual];

        //Ajusta las barras del slider
        sliderVida.value = normalizaValorSlider(listaNaves[naveActual].vida);
        sliderAtaque.value = normalizaValorSlider(listaNaves[naveActual].ataque);
        sliderVelocidad.value = normalizaValorSlider(listaNaves[naveActual].velocidad);

        //Ajusta los colores del slider en funcion de los valores
        rellenoSliderVida.color = getColorSlider(sliderVida.value);
        rellenoSliderAtaque.color = getColorSlider(sliderAtaque.value);
        rellenoSliderVelocidad.color = getColorSlider(sliderVelocidad.value);

        //Debug.LogWarning();
    }

    private Color getColorSlider(float value)
    {
        Color color = Color.red;

        //if (value > 0.7) color = Color.green;
        //if (value <= 0.7) color = new Color(174, 221, 4);//Verde claro
        if (value <= 1) color = Color.green;
        if (value <= 0.55) color = Color.yellow;
        if (value <= 0.2) color = Color.red;

        //if (value <= 0.4) color = new Color(221, 145, 4);//Naranja claro
        //if (value > 0.1 && value <= 0.2) color = new Color(221, 98, 4); //Naranja oscuro

        //Debug.Log(color);

        return color;
    }

    //Convierte el valor a uno valido apra el slider. Siendo 6 el 100% del slider
    private float normalizaValorSlider(float valor)
    {
        return valor/6;
    }

    public void empezarJuego()
    {
        gameObject.SetActive(false);
        Vector2 posicionInicio = new Vector2(0,-3.5f);
        //Instancia la nave generica
        GameObject naveJugador = Instantiate(prefabNave,posicionInicio,Quaternion.identity) as GameObject;
        
        //Coge el script del comportamientio para modificar el valor de velocidad y asignarle sus ataques correspondientes
        jugadorClienteController scriptJugador = naveJugador.GetComponent<jugadorClienteController>();
        //Cambia los valores
        scriptJugador.velocidad = listaNaves[naveActual].velocidad;
        scriptJugador.disparos = listaNaves[naveActual].disparos;
        //Cambia el sprite
        scriptJugador.spriteNave.sprite =  listaNaves[naveActual].spriteNave;
        //Asigna los sprites de mejora para cunado coja un powerup
        scriptJugador.listaSpritesMejoras = listaNaves[naveActual].listaSpritesMejoras;

        //Activa la música de fondo
        audioManager.Play(musicaDeFondo);
        //Activa el "mapa de enemigos" y sus secuencias/generadores de enemigos
        mapaJuego.SetActive(true);
    }
}
