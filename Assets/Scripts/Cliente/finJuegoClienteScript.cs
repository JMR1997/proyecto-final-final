﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class finJuegoClienteScript : MonoBehaviour
{
    public string[] frasesLocas;
    public TextMeshProUGUI fraseRandom;
    public TextMeshProUGUI textoScore;
    public TextMeshProUGUI textoHighscore;

    private GameManagerCliente gm;
    private string claveScore = "Max Score NE";
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindObjectOfType<GameManagerCliente>();

        int numFraseAlea = UnityEngine.Random.Range(0,frasesLocas.Length);
        fraseRandom.text = frasesLocas[numFraseAlea];
        textoScore.text = gm.score.ToString();
        textoHighscore.text = String.Format("{0:0000000000}", _FuncionesComunes.GetMaxScore(claveScore));
        /*
        int highscore = PlayerPrefs.Load("navesHighscore");
        int scoreEstaPartida = gm.textoScore.text;
        if (scoreEstaPartida > highscore) {
            PlayerPrefs.Save("navesHighscore", scoreEstaPartida);
            highscore = scoreEstaPartida;
        }
        textoHighscore.text = highscore;
        */
    }
}
