﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivaEnemigosController : MonoBehaviour
{
    public bool activaTodosALaVez = false;
    public float retrasoEntreHijos = 0.2f;
    private GameObject[] listaHijos;
    // Start is called before the first frame update
    void Start()
    {
        if (activaTodosALaVez)
        {
            activaHijosAlaVezDelay();
        }
        else
        {
            activaHijosAlDestruir();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (!activaTodosALaVez)
        {
            if (transform.childCount != listaHijos.Length)
            {
                activaHijosAlDestruir();
            }
        }
        
    }

    private void activaHijosAlDestruir()
    {
        listaHijos = new GameObject[transform.childCount];
        for (int i = 0; i < listaHijos.Length; i++)
        {
            listaHijos[i] = transform.GetChild(i).gameObject;
            listaHijos[i].SetActive(false);
        }
        //Lo activa e incrementa
        if(listaHijos.Length >0) listaHijos[0].SetActive(true);
    }

    private void activaHijosAlaVezDelay()
    {
        listaHijos = new GameObject[transform.childCount];
        for (int i = 0; i < transform.childCount; i++)
        {
            listaHijos[i] = transform.GetChild(i).gameObject;
            StartCoroutine(activaConDelay(listaHijos[i], i * retrasoEntreHijos));
        }
    }

    IEnumerator activaConDelay(GameObject objetoEnemigo, float tiempoParaActivar)
    {
        objetoEnemigo.SetActive(false);
        float tiempoPasado = 0f;
        while (tiempoPasado < tiempoParaActivar)
        {
            tiempoPasado += Time.deltaTime;
            yield return null;
        }

        objetoEnemigo.SetActive(true);
        
    }

    private void OnDestroy()
    {
        GameObject objetoHermano;
        int indexSiguienteHermano = transform.GetSiblingIndex()+1;
        
        //Si no es el ultimo hermano/hijo de la jerarquia activa al siguiente
        if (indexSiguienteHermano < transform.parent.childCount)
        {
            objetoHermano = transform.parent.GetChild(indexSiguienteHermano).gameObject;
            objetoHermano.SetActive(true);
        }
    }


}
