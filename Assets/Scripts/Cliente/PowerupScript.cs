﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum Potenciador
{
    Ninguno = 0,
    DisparoDoble = 1,
    DisparoContinui = 2,
    DisparoCono = 3,
    DisparoMisil = 4
}

public class PowerupScript : MonoBehaviour
{
    Animator anim;
    [Tooltip("Al tiempo indicado se le suman 3.5 de la animacion de desaparecer")]
    public float tiempoDesaparecer = 3f;
    private float tiempoRes;
    private jugadorClienteController jugador;
    [Tooltip("Ninguno(malo),Doble,escudo,cono,metralleta")]
    public Sprite[] spritesPowerUp;
    public SpriteRenderer sp;
    public Potenciador tipoPowerUp = Potenciador.DisparoDoble;
    
    private AudioManager am;
    public string nombreSonido = "Powerup";
    // Start is called before the first frame update
    void Start()
    {
        am = GameObject.FindObjectOfType<AudioManager>();
        sp = gameObject.GetComponent<SpriteRenderer>();
        tiempoRes = tiempoDesaparecer;
        anim = GetComponent<Animator>();
        jugador = GameObject.FindObjectOfType<jugadorClienteController>();
    }

    public void setTipoPowerUp(Potenciador tipoPowerUp)
    {
        this.tipoPowerUp = tipoPowerUp;
        sp.sprite = spritesPowerUp[(int)tipoPowerUp];
    }

    public void setTiemmpoDesaparecer(float tiempoDesaparecer)
    {
        this.tiempoDesaparecer = tiempoDesaparecer;
        this.tiempoRes = tiempoDesaparecer;
    }

    private void Update()
    {
        tiempoRes -= Time.deltaTime;
        AnimatorStateInfo estadoAnim = anim.GetCurrentAnimatorStateInfo(0);

        if (estadoAnim.IsName("desaparecer"))
        {
            if (estadoAnim.normalizedTime > 0.9)
            {
                Destroy(gameObject);
            }
        }
        else
        {
            if (tiempoRes < 0)
            {
                anim.Play("desaparecer");
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Destructor"))
        {
            Destroy(gameObject);
        }else if (collision.CompareTag("Player"))
        {
            jugador.setPotenciador(this.tipoPowerUp);
            am.Play(nombreSonido);
            Destroy(gameObject);
        }
    }
}
