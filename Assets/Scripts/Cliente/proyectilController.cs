﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class proyectilController : MonoBehaviour
{
    public float velocidadMovimiento = 1f;
    public float cantidadDanio = 1f;
    public bool esEnemigo = false;
    public GameObject particulas;
    public bool destruirAlChocar = true;

    [Header("Retardo para activar")]
    public bool tieneRetardo = false;
    public float tiempoRetardo = 0f;
    private float tiempoPasado;
    private SpriteRenderer sp;

    private void Start()
    {
        sp = gameObject.GetComponent<SpriteRenderer>();
        if(tieneRetardo) tiempoPasado = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (tieneRetardo && tiempoPasado<tiempoRetardo)
        {
            tiempoPasado += Time.deltaTime;
            //Oculta el sprite y no mueve el proyectil
            sp.enabled = false;
        }
        else
        {
            //Muestra el sprite
            sp.enabled = true;
            //Mueve el proyectil
            transform.Translate(Vector2.up * velocidadMovimiento * Time.deltaTime);
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        recibeDanio script = collision.gameObject.GetComponent<recibeDanio>();
        if(!esEnemigo && script != null)
        {
            script.recibirDanio(cantidadDanio);
            if(destruirAlChocar) destruirProyectil();
        }else if (esEnemigo && collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.SendMessage("quitarVida",1);
            if (destruirAlChocar) destruirProyectil();
        }
    }

    private void destruirProyectil()
    {
        //Spawnea particulas
        if(particulas != null)
        {
            Instantiate(particulas, transform.position, Quaternion.identity);
        }
        Destroy(gameObject);
    }

    private void OnBecameInvisible()
    {
        if (tieneRetardo && tiempoPasado < tiempoRetardo)
        {
            //Evita que se destruyan los proyectiles con retardo antes de que se activen
        }
        else
        {
            Destroy(gameObject);
        }
    }


}
