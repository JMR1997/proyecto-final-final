﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;
using UnityEngine.SceneManagement;

public class GameManagerCliente : MonoBehaviour
{
    [Header("User Interface")]
    public TextMeshProUGUI textoScore;
    public TextMeshProUGUI textoVida;
    public int score;
    public int vida;
    public int vidaMax = 99;

    public GameObject menuFinDePartida;
    public GameObject mapaDeJuego;

    private int multiplicador;

    [Header("Tiempo y modo de juego")]
    public float tiempoRalentizado = 0.2f;

    
    public bool _isModoRalentizado;
    public bool _isDisparoAutomatico;


    //Musica
    private AudioManager audioManager;
    private string bgMusic = "BgMusic";
    //Puntos
    private string claveScore = "Max Score NE";

    // Start is called before the first frame update
    void Start()
    {
        multiplicador = 1;
        textoScore.text = String.Format("{0:0000000}", score);
        textoVida.text = vida + "";

        estaTiempoRalentizado = _isModoRalentizado;
        audioManager = FindObjectOfType<AudioManager>();

        if(Screen.currentResolution.width > Screen.currentResolution.height)
        {
            Screen.SetResolution(Screen.currentResolution.height, Screen.currentResolution.width,true);
        }
    }

    // Update is called once per frame
    
    void Update()
    {
        _FuncionesComunes.ComprobarCerrarAplicacion();
    }
    

    public int getScore()
    {
        return this.score;
    }

    public void cambiarModoTiempo(bool value) {
        //Esta variable es para saber que modo esta en funcionamiento
        this._isModoRalentizado = value;
        //Cambia esta variable para que cambie el tiempo
        estaTiempoRalentizado = _isModoRalentizado;
    }

    //Metodos getter y setter. Si se cambia el valor se cambia el tiempo/timeScale al que funcioan el juego
    public bool estaTiempoRalentizado
    {
        get { return _isModoRalentizado; }
        set
        {
            //Solo actualiza el tiempo cuando cambia esta variable
            if (value == false)
            {
                Time.timeScale = 1f;
            }
            else
            {
                Time.timeScale = tiempoRalentizado;
            }
        }
    }

    //Metodos getter y setter. Si se cambia el valor se cambia el modo de disparo. El script del jugador lo comprubea en cada update
    public bool isDisparoAutomatico { get; set; }

    public void aumentarPuntuacion(int cantidad)
    {
        score += cantidad;
        textoScore.text = String.Format("{0:000000}", score);
        if (score > _FuncionesComunes.GetMaxScore(claveScore))
        {
            _FuncionesComunes.SaveScore(claveScore, score);
        }
    }

    public void aumentarVida(int cantidad)
    {
        vida += cantidad;
        if (vida > 99) vida = 99;
        textoVida.text = vida + "";
    }

    public void restarVida(int cantidad)
    {
        vida -= cantidad;
        if (vida > 99) vida = 99;
        textoVida.text = vida + "";
        
        //if (vida <= 0) finJuego();
    }

    public int getMultiplicador()
    {
        return this.multiplicador;
    }

    internal bool quedanVidas()
    {
        return (vida > 0);
    }

    internal void finJuego()
    {
        Time.timeScale = 1f; //Poner tiempo a velocidad normal
        //Aparece pantalla de fin del juego chula con sus sonidos y todo pro. Quizá con un objeto timeline?
        menuFinDePartida.SetActive(true);
        //mapaDeJuego.SetActive(false);
        audioManager.Stop(bgMusic);
        //Mostrar highscore local y quiza online?? Boton de volver a jugar reinicia escena y boton de ir atras vuelve al menu
    }

    public void recargarEscena()
    {
        Time.timeScale = 1f; //Poner tiempo a velocidad normal
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void volverAtras()
    {
        Time.timeScale = 1f; //Poner tiempo a velocidad normal
        _FuncionesComunes.CargarEscena("Menu");
    }
}

