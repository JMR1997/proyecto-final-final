﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstantiateAlDestruir : MonoBehaviour
{
    public GameObject objetoInstanciar;
    public int rotacion = 0;

    private void OnDestroy()
    {
        GameObject instancia = Instantiate(objetoInstanciar,transform.position,Quaternion.Euler(0,0,rotacion));
        instancia.transform.parent = transform.parent;
        Destroy(gameObject);
    }

}
