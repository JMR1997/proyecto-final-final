﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jugadorMuereFinAnim : StateMachineBehaviour
{
    //public GameObject prefabJugador;

    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    //override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
    //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    
    //}

    // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        GameManagerCliente gm = GameObject.FindObjectOfType<GameManagerCliente>();
        //El script jugadorController ya restó la vida al iniciar la animacion
        if (gm.quedanVidas())
        {
            Vector2 pos = new Vector2(0,-3.5f);
            //Instancia un nuevo objeto jugador
            GameObject objetoJugador =  Instantiate(animator.gameObject, pos, Quaternion.identity);
            objetoJugador.GetComponentInChildren<jugadorClienteController>().setPotenciador(Potenciador.Ninguno);
            //Borra esta instancia
            Destroy(animator.gameObject);
        }
        else
        {
            gm.finJuego();
            Destroy(animator.gameObject);
        }
    }

    // OnStateMove is called right after Animator.OnAnimatorMove()
    //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that processes and affects root motion
    //}

    // OnStateIK is called right after Animator.OnAnimatorIK()
    //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    //{
    //    // Implement code that sets up animation IK (inverse kinematics)
    //}
}
