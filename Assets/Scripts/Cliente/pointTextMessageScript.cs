﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class pointTextMessageScript : MonoBehaviour
{
    private TextMeshProUGUI texto;
    private int _puntos;
    public Color colorMenos100;
    public Color colorMenos200;
    public Color colorMenos500;
    public Color colorMax;
    // Start is called before the first frame update
    private void Awake()
    {
        texto = gameObject.GetComponentInChildren<TextMeshProUGUI>();
    }

    public int puntos
    {
        get { return _puntos;}
        set
        {
            VertexGradient colorDegradado = texto.colorGradient;
            _puntos = value;
            if (_puntos >= 50)  {
                colorDegradado.bottomRight = colorMenos100;
                colorDegradado.bottomLeft = colorMenos100;
            }
            if (_puntos >= 100) {
                colorDegradado.bottomRight = colorMenos200;
                colorDegradado.bottomLeft = colorMenos200;
            }
            if (_puntos >= 200) {
                colorDegradado.bottomRight = colorMenos500;
                colorDegradado.bottomLeft = colorMenos500;
            }
            if (_puntos >= 500) {
                colorDegradado.bottomRight = colorMax;
                colorDegradado.bottomLeft = colorMax;
            }

            texto.colorGradient=colorDegradado;
            texto.text = _puntos.ToString();
        }
    }
}
