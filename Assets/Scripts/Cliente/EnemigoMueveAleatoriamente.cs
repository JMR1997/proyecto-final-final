﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemigoMueveAleatoriamente : MonoBehaviour
{
    public float velocidad = 1f;
    private Vector2 posOriginal;
    private Vector2 posObjetivo;
    // Start is called before the first frame update
    void Start()
    {
        posOriginal = transform.position;
        posObjetivo = buscaPosicionAleatoria();
    }

    // Update is called once per frame
    void Update()
    {
        moverAleatoriamente();
    }

    private void moverAleatoriamente()
    {
        float distancia = Vector2.Distance(transform.position, posObjetivo);
        if (distancia > 1) //Se mueve
        {
            transform.position = Vector2.MoveTowards(transform.position, posObjetivo, velocidad * Time.deltaTime);
        }
        else
        {
            //Cambia de objetivo
            posObjetivo = buscaPosicionAleatoria();
        }
    }

    //Genera uan posicion aleatoria en la indicada por el marco de posiciones proporcionado
    private Vector2 buscaPosicionAleatoria()
    {
        float yRandom, xRandom;
        //Cogido a mano de las posiciones del mundo
        float maximoDer = 2.75f;
        float maximoIzq = -2.75f;

        float maximoArriba = 0.6f;
        float maximoAbajo = -4f;

        xRandom = Random.Range(maximoIzq, maximoDer);
        yRandom = Random.Range(maximoArriba, maximoAbajo);


        Vector2 pos = new Vector2(xRandom,yRandom);
        return pos;
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position,posObjetivo);
    }
}
