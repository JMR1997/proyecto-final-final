﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu (menuName = "NaveJugadorCliente")]
public class JugadorClienteScriptable : ScriptableObject
{

    public string nombre = "Default";
    //Valor inventado
    public float vida = 4f;
    //Valor real que se usa para el gameobject
    public float velocidad = 4.7f;
    //Valor inventado
    public float ataque = 3f;

    public GameObject[] disparos;
    public Sprite spriteNave;
    public Sprite[] listaSpritesMejoras;
    public Vector2 posicionDisparo;

}
