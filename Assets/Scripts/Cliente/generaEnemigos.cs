﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//Para que no haya ambigüedad
using Random = UnityEngine.Random;

public class generaEnemigos : MonoBehaviour
{
    public Transform posTopIzq;
    public Transform posBotDer;

    //Si hay enemigos en cola para generarse pero aun no han sido generador esta variable será true
    //Evita que se sigan spwneando enemigos mientras se espera que aparezcan los encolados
    //Arregla ese fallo cuando los enemigos aparecen ocn tiempo de retraso
    private bool enemigosEnCola;

    [Header("Retardo ALEATORIO al generar enemigos - o ninguno(0,0)")]
    public float retardoMinGenerarEnemigo = 0f;
    public float retardoMaxGenerarEnemigo = 3f;

    [Header("Minimo enemigos para generar nuevos")]
    [Tooltip("Cuando alcanza el número genera más enemigos")]
    public float minEnemVivos = 1f;

    public GameObject[] posiblesEnemigos;
    public int maxEnemAlea = 1;

    private GameManagerCliente gm;
    public int puntosAlcanzar;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindObjectOfType<GameManagerCliente>();
    }

    // Update is called once per frame
    void Update()
    {
        //Siempre tiene dos hijos que son las posiciones del cuadrado en el que se permite generar enemigos
        //Se le suma la cantidad minima de enemigos vivos
        //Asi cuando llega al numero minimo o inferior genera enemigos nuevos
        if (transform.childCount <= 2+minEnemVivos)
        {
            if(gm.getScore() < puntosAlcanzar)
            {
                //Si hay enemigos esperando spawnear no genera una nueva ronda
                //Si no los hay genera una nueva ronda de enemigos
                if (!enemigosEnCola)
                {
                    generaEnemigosAleatorios();
                }
            }
            else
            {
                //Genera uun clon de este spawner de enemigos pero incrementa la puntuacion limite. Lo asocia al objeto padre de este(seria un hermano de este)
                GameObject nuevoSpaawner = Instantiate(gameObject,transform.position,Quaternion.identity,transform.parent);
                generaEnemigos scriptNuevoSpawner = nuevoSpaawner.GetComponent<generaEnemigos>();
                scriptNuevoSpawner.puntosAlcanzar = (int)Math.Floor(this.puntosAlcanzar * Random.Range(1.25f,2f));
                scriptNuevoSpawner.minEnemVivos = Random.Range(1, 3);
                scriptNuevoSpawner.maxEnemAlea = Random.Range(2, 5);
                //Destruye esta instancia
                Destroy(gameObject);
            }
            
        }//else nada, esperar a que el jugador mate los enemigos o se maten solos en el destructor
    }

    private void generaEnemigosAleatorios()
    {
        if (posiblesEnemigos.Length > 0)
        {
            //Genera un numero aleatorio de enemigos
            float numAlea = 1;
            if(maxEnemAlea != 1)
            {
                numAlea = Random.Range(1, this.maxEnemAlea);
            }

            //Genera tantos enemigos como haya salido del random
            for (int i = 0; i < numAlea; i++)
            {
                //Genera unas nuevas coordenadas
                float posX = Random.Range(posTopIzq.position.x, posBotDer.position.x);
                float posY = Random.Range(posTopIzq.position.y, posBotDer.position.y);
                Vector2 posEnemigo = new Vector2(posX, posY);

                float tiempoRetardo = Random.Range(this.retardoMinGenerarEnemigo, this.retardoMaxGenerarEnemigo);

                StartCoroutine(spawneaEnemigo(posEnemigo, tiempoRetardo));
            }
        }
        else
        {
            Debug.LogError("te has olvidado de meter enemigos en el generador");
        }
    }

    /*
     * Genera un enemigo en la pos pasada y con el tiempo de retardo pasado por parametro
     */
    private IEnumerator spawneaEnemigo(Vector2 posEnemigo, float tiempoRetardo)
    {
        //Encola el enemigo y se asegura que no se generen nuevas rondas de enemigos mientras carga la ronda actual
        this.enemigosEnCola = true;

        //Se espera el tiempo aleatorio calculado
        yield return new WaitForSeconds(tiempoRetardo);

        //Pasada la espera... ejecuta codigo

        //Coge un enemiigo de una posicion del array aleatoria
        int posArrayAlea = (int)Random.Range(0, this.posiblesEnemigos.Length);
        //Los spawnea
        Instantiate(posiblesEnemigos[posArrayAlea], posEnemigo, Quaternion.identity, gameObject.transform);

        //Ya ha generado un enemmigo puede indicar que no hay en cola aunque los haya porque
        //ya hay un enemigo hijo del objeto y no se generaran más rondas
        this.enemigosEnCola = false;
    }

    private void OnDrawGizmosSelected()
    {
        if(posTopIzq != null && posBotDer != null)
        {
            Gizmos.color = Color.yellow;

            //Diagonal
            Gizmos.DrawLine(posTopIzq.position, posBotDer.position);
            Gizmos.DrawLine(new Vector2(posTopIzq.position.x, posBotDer.position.y), new Vector2(posBotDer.position.x, posTopIzq.position.y));
            //Lados
            Gizmos.DrawLine(posTopIzq.position, new Vector2(posBotDer.position.x, posTopIzq.position.y));
            Gizmos.DrawLine(new Vector2(posTopIzq.position.x, posBotDer.position.y), new Vector2(posBotDer.position.x, posBotDer.position.y));
            Gizmos.DrawLine(new Vector2(posTopIzq.position.x, posTopIzq.position.y), new Vector2(posTopIzq.position.x, posBotDer.position.y));
            Gizmos.DrawLine(new Vector2(posBotDer.position.x, posTopIzq.position.y), new Vector2(posBotDer.position.x, posBotDer.position.y));
        }
    }
}
