﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidController : MonoBehaviour
{
    public GameObject asteroideClon;
    public float velocidadRotacin = 0.5f;

    private SpriteRenderer sp;
    public Sprite[] posiblesSprite;
    [Header("Instancia trozos de asteroide")]
    public int minAsteroides = 2;
    public int maxAsteroides = 5;

    // Start is called before the first frame update
    void Start()
    {
        //Elige un sprite aleatorio de la lista
        sp = gameObject.GetComponent<SpriteRenderer>();
        int spriteAlea = Random.Range(0,posiblesSprite.Length);
        sp.sprite = posiblesSprite[spriteAlea];

        //Regenera el polygon collider para que se adapte al sprite elegido del catalogo
        Destroy(gameObject.GetComponent<PolygonCollider2D>());
        PolygonCollider2D col = gameObject.AddComponent<PolygonCollider2D>();
        col.isTrigger = true;
    }

    private void OnDestroy()
    {
        //Calcula cuantos trozos de asteroide va a generar
        int numAlea = Random.Range(minAsteroides, maxAsteroides);
        Vector3 escalaHijos = new Vector3(transform.localScale.x, transform.localScale.y, transform.localScale.z);
        escalaHijos = escalaHijos / 2;

        for (int i = 0; i < numAlea; i++)
        {
            GameObject asteroide = Instantiate(asteroideClon,transform.position,Quaternion.identity);
            asteroide.transform.localScale = escalaHijos;

            Vector3 rotacionAlea = new Vector3(0,0, Random.Range(0,360));
            asteroide.transform.Rotate(rotacionAlea);

            asteroide.GetComponentInChildren<proyectilController>().velocidadMovimiento *= Random.Range(1.5f,4f);//Aumenta la velocidad del proyectil Entre un 150% y un 400%;

        }

    }
}
