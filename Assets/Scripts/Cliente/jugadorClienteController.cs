﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*
    enum Potenciador

    Ninguno = 0,
    DisparoDoble = 1,
    DisparoContinui = 2,
    DisparoCono = 3,
    DisparoMisil = 4

     */

public class jugadorClienteController : MonoBehaviour
{
    public float velocidad = 3f;
    private Camera cam;

    [Header("Vida")]
    public int vidaMax = 5;
    [SerializeField]
    private int vidaActual;

    [Header("Proyectiles")]
    public GameObject[] disparos;
    public Transform posicionDisparo;

    [Header("Recarga disparo")]
    public float[] tiempoEntreAtaques;
    private float tiempoResAtacar;

    private Vector2 posicionObjetivo;
    private Animator anim;
    private Collider2D colisionador;
    private GameManagerCliente gm;
    private AudioManager am;

    [Header("Particulas motor")]
    //Particulas de trayectoria
    private ParticleSystem particulas;
    public float vidaParticulasIdle = 0.1f;
    public float vidaParticulasMovimiento = 0.35f;
    [Header("Sprites nave")]
    public SpriteRenderer spriteNave;
    private SpriteRenderer spriteColisionador;
    public SpriteRenderer spriteMejoras;
    public Sprite[] listaSpritesMejoras;

    //Es una constante, pero Vector2 no permite ser constante
    private Vector2 posicionNull = new Vector2(9999, 9999);
    //Controla si se ralentiza el tiempo al o haber input del jugador
    private bool modoRalentizado;
    //Controla si está el modo en el que siempre intenta disparar automaticamente
    private bool disparoAutomatico;
    public Potenciador potenciadorActivo;
    AnimatorStateInfo infoAnim;
    // Start is called before the first frame update
    void Start()
    {
        particulas = gameObject.GetComponentInChildren<ParticleSystem>();
        am = GameObject.FindObjectOfType<AudioManager>();
        gm = GameObject.FindObjectOfType<GameManagerCliente>();

        //Es necesario tener este sprite en el objeto para que se calcule bien el collider
        if (gameObject.GetComponent<SpriteRenderer>() == null)
        {
            SpriteRenderer spriteColisionador = gameObject.AddComponent<SpriteRenderer>();
            spriteColisionador.sprite = spriteNave.sprite;
            //Pero se puede desactivar apra que el usuario nmo vea nada
            spriteColisionador.enabled = false;
        }

        //Resetea el colisionador para que se recalcule su forma el iniciar. Asi no depende de sprite, se calcula en tiempo de ejecucion
        Destroy(GetComponent<PolygonCollider2D>());
        colisionador = gameObject.AddComponent<PolygonCollider2D>();

        anim = gameObject.GetComponent<Animator>();
        
        cam = Camera.main;
        vidaActual = vidaMax;
        //potenciadorActivo = Potenciador.Ninguno;
        tiempoResAtacar = 0;

        //Porque vector2 o puede ser null usamos este valor como valor nulo, no válido
        posicionObjetivo = posicionNull;
        //Al principio siempre va a velocidad normal - "modo Escritorio"
        modoRalentizado = false;

    }

    // Update is called once per frame
    void Update()
    {
        infoAnim = anim.GetCurrentAnimatorStateInfo(0);

        //Variable que si estamos en "modo ralentizado" ralentiza el tiempo cuando no hay input
        //Si hay input el tiempo transcurre a velocidad normal
        bool hayInputJugador = false;
        this.disparoAutomatico = gm.isDisparoAutomatico;
        this.modoRalentizado = gm.estaTiempoRalentizado;
        


        //INPUT JUGADOR
        //Movimiento con teclas/joystick
        if (Input.GetAxisRaw("Vertical") != 0 || Input.GetAxisRaw("Horizontal") != 0)
        {
            //Coge la posicion actual
            posicionObjetivo = transform.position;
            //Le suma el input del jugador para que se desplace a donde el jugador manda
            posicionObjetivo.x += Input.GetAxis("Horizontal");
            posicionObjetivo.y += Input.GetAxis("Vertical");
            //Más abajo está el codigo para moverlo aqui solo calcula la posicion

            //Para controlar que si estamos en modo ralentizado el tiempo vaya a velocidad normal
            hayInputJugador = true;
        }
        
        //Controla el enfriamiento entre disparos
        if(tiempoResAtacar > 0)
        {
            tiempoResAtacar -= Time.deltaTime;
        }

        //Mientras se presione el clic o el boton de saltar(espacio) INTENTA disparar. Si está en enfriamiento no lo hace.
        if (Input.GetMouseButton(0) || Input.GetButton("Jump") || disparoAutomatico)
        {
            //Ataca con click o boton atacar
            intentaAtacar();
            //Si hay click marca como objetivo la posicion del raton/dedo que ha hecho click
            if (Input.GetMouseButton(0))
            {
                posicionObjetivo = cam.ScreenToWorldPoint(Input.mousePosition);
                hayInputJugador = true;
            }else if (Input.GetButton("Jump"))
            {
                hayInputJugador = true;
            }///else esta en disparo automaticvvo
            

            //Para controlar que si estamos en modo ralentizado el tiempo vaya a velocidad normal
        }

        //Cambia el modo con el Fire3 (Shift izquierdo en teclado) 
        //TODO cambiar Fire3 para que sea el boton select en mando
        if (Input.GetButtonDown("Fire3")) this.modoRalentizado = !modoRalentizado;

        //-----------------------------------------------------------------------------

        //MOVIMIENTO JUGADOR
        //Si no es cero es que son distintos y tiene una posicion objetivo válida. Solo se usaa al iniciar el objeto. Una vez elegida la primera posicion siempre se cumple
        if (Vector2.Distance(posicionObjetivo, posicionNull) != 0 && posicionObjetivo != posicionNull)
        {
            //Particuals de las aprticuals del motor trayectoria
            //Coge los parametros del sistema de particulas para poder modificarlo segun se esté moviendo o no la nave
            ParticleSystem.MainModule main = particulas.main;


            if (Vector2.Distance(posicionObjetivo, transform.position) == 0)
            {
                //Ha llegado al punto
                //Modifica poco a poco para bajar la vida de las particulas a la de idle
                //A menos vida tienen mas corta es la trayectoria/cola de la nave
                float particulasActuales = main.startLifetime.constant;
                main.startLifetime = Mathf.Lerp(particulasActuales, vidaParticulasIdle, Time.deltaTime*3.5f);
                //Multiplica deltaTime x3 para que sea una transicion bastante más rapida, es un valor basado en prueba y error a ver que quedaba bien
            }
            else
            {
                if (!(infoAnim.IsName("jugadorMuere") || infoAnim.IsName("jugadorEntra")))
                { //Si está entrando o muriendo no puede moverse
                  //mover al punto
                    transform.position = Vector2.MoveTowards(transform.position, posicionObjetivo, velocidad * Time.deltaTime);

                    //Recorta la posicion a una adecuada por si fuese necesario
                    Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
                    pos.x = Mathf.Clamp01(pos.x);
                    pos.y = Mathf.Clamp01(pos.y);
                    transform.position = Camera.main.ViewportToWorldPoint(pos);
                }
                //Modifica poco a poco para subir la vida de las particulas mientras se está moviendo la nave
                //A más vida tienen mas larga es la trayectoria/cola de la nave
                float particulasActuales = main.startLifetime.constant;
                main.startLifetime = Mathf.Lerp(particulasActuales, vidaParticulasMovimiento, Time.deltaTime*2);
                //Multiplica deltaTime x2 para que sea una transicion más rapida
            }

        }

        //-----------------------------------------------------------------------------
        //Comprueba si está jugando en el modo ralentizado en el que se ralentiza el tiempo si el jugaodrn o pulsa nada
        if (modoRalentizado)
        {
            if (hayInputJugador) gm.estaTiempoRalentizado = false;
            else gm.estaTiempoRalentizado = true; //Velocidad ralentizada
        }
        else
        {
            //Va a velocidad normal
            gm.estaTiempoRalentizado = false;
        }


    }

    private void intentaAtacar()
    {
        if(tiempoResAtacar <= 0)
        {
            if (!(infoAnim.IsName("jugadorMuere") || infoAnim.IsName("jugadorEntra"))) //Si está entrando o muriendo no puede disparar
            {
                Instantiate(disparos[(int)potenciadorActivo], posicionDisparo.position, Quaternion.identity);
                tiempoResAtacar = tiempoEntreAtaques[(int)potenciadorActivo];
            }
        }
    }

    private void quitarVida(int cantidad)
    {
        Debug.Log("jugador herido");
        this.vidaActual -= cantidad;
        if (vidaActual < 0)
        {
            jugadorMuere();
        }
        else
        {
            StartCoroutine(jugadorParpadea());
        }
    }

    //Se llama cuando el jugador pierde HP
    private IEnumerator jugadorParpadea()
    {
        //Poner sprite en rojo unos segundos??

        colisionador.enabled = false;
        //Parpde para mostrr al usuario que es invulnerable pero se queda en la misma posición
        anim.Play("parpadea");
        yield return new WaitForSeconds(1f);
        //Deja de parpadear y reactiva el colisionador
        anim.Play("idle");
        colisionador.enabled = true;
    }
    

    //Se llama cuando el jugador ha perdido una vida
    private void jugadorMuere()
    {
        gm.restarVida(1);
        colisionador.enabled = false;
        anim.Play("jugadorMuere");
        //animador transiciona a entrada jugador (y sale una nueva nave de debajo)
        //Destruye esta instancia de  jugador
    }

    public void setPotenciador(Potenciador p)
    {
        this.potenciadorActivo = p;
    }
}
