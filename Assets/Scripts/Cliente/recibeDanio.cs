﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class recibeDanio : MonoBehaviour
{
    public int vidaMax;
    [SerializeField]
    private float vidaActual;
    public bool destruirPadre = false;
    private bool destruidoPorJugador;

    public int puntos = 100;
    private GameManagerCliente gm;
    private AudioManager audioManager;
    public string nombreSonidoMuerte = "ExplosionPeq";
    public string nombreSonidoDanio = "Impacto";
    public GameObject textoPuntosSpawn;

    //public bool parpadearDanio = true;
    private SpriteRenderer sp;

    public GameObject particulasExplotar;

    [Header("Powerup")]
    public GameObject objetoPowerUp;
    [Range(0f,100f)]
    public float posibilidadSoltarPowerUp = 5f;
    [Header("% drop powerups")]
    [Range(0f, 100f)]
    public float posDispDoble = 25f;
    [Range(0f, 100f)]
    public float posDispContinuo = 25f;
    [Range(0f, 100f)]
    public float posDispCono = 25f;
    [Range(0f, 100f)]
    public float posDispMisil = 25f;
    


    // Start is called before the first frame update
    void Start()
    {
        audioManager = GameObject.FindObjectOfType<AudioManager>();
        gm = GameObject.FindObjectOfType<GameManagerCliente>();


        destruidoPorJugador = true;
        vidaActual = vidaMax;
        /*
        if (parpadearDanio)
        {
            sp = GetComponent<SpriteRenderer>();
        }
        */
    }

    public void recibirDanio(float cantidad)
    {
        //StartCoroutine(parpadeoSprite(sp));
        this.vidaActual -= cantidad;
        if (this.vidaActual <= 0)
        {

            if (destruidoPorJugador)
            {
                try
                {
                    //Aumenta puntos jugador
                    gm.aumentarPuntuacion(puntos * gm.getMultiplicador());


                    //Instancia el texto con los puntos que ha dado
                    GameObject instanciaTexto = Instantiate(textoPuntosSpawn, transform.position, Quaternion.identity);
                    //Le asigna el valor de puntos al script del objeto y este ya hace la magia
                    instanciaTexto.GetComponentInChildren<pointTextMessageScript>().puntos = puntos * gm.getMultiplicador();
                    //Instancia particulas explotar
                    Instantiate(particulasExplotar,transform.position,Quaternion.identity);

                    Potenciador powerUpAlea = calculaDropAleatorio();
                    if(powerUpAlea != Potenciador.Ninguno)
                    {
                        GameObject instanciaPowerUp = Instantiate(objetoPowerUp,transform.position,Quaternion.identity);
                        PowerupScript scriptPU = instanciaPowerUp.GetComponentInChildren<PowerupScript>();
                        scriptPU.setTipoPowerUp(powerUpAlea);
                    }

                    //Reproduce sonido
                    audioManager.Play(nombreSonidoMuerte);
                }
                catch(Exception e)
                {

                }

            }


            try
            {
                if (destruirPadre)
                {
                Destroy(transform.parent.gameObject);
                }
                Destroy(this.gameObject);

            }catch(InvalidOperationException e) { }//Ocurre esta excepcion cuando se destruye un elemento dentro de un prefab

        }
        else
        {
            audioManager.Play(nombreSonidoDanio);
        }
    }

    //Calcula si cae un powerup o no.
    //En caso de que si calcula de que tipo corresponde
    //En caso de que no devuelve Potenciador.Ninguno
    private Potenciador calculaDropAleatorio()
    {
        Potenciador p = Potenciador.Ninguno;

        float numAlea = UnityEngine.Random.Range(0,100);
        if(numAlea <= posibilidadSoltarPowerUp)
        {
            //Hay powerup

            //Suma todos los porcentajes y les asigna un rango a cada powerup. Si cae en el rango de uno se isntancia ese
            float rangoMax = posDispContinuo + posDispCono + posDispDoble + posDispMisil;
            numAlea = UnityEngine.Random.Range(0,rangoMax);

            //Va de mas a menos comprobando. 
            if (numAlea < rangoMax) p = Potenciador.DisparoMisil;
            if (numAlea < (rangoMax-posDispMisil)) p = Potenciador.DisparoDoble;
            if (numAlea < (rangoMax - posDispMisil-posDispCono)) p = Potenciador.DisparoCono;
            if (numAlea < posDispContinuo) p = Potenciador.DisparoContinui;
        }

        return p;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Destructor"))
        {
            //Destruye el objeto
            destruidoPorJugador = false;
            recibirDanio(9999);
        }
    }

    IEnumerator parpadeoSprite(SpriteRenderer render)
    {
        Color colorAnterior = Color.white;
        try
        {
             colorAnterior = sp.color;
            sp.color = Color.red;
        }
        catch (Exception e) { }

        yield return new WaitForSeconds(0.3f);

        try
        {
            sp.color = colorAnterior;
        }
        catch (Exception e) { }

    }
}

