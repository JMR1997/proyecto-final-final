﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MessageGenerator : MonoBehaviour
{

    private Text textoCampo;
    private Animator anim;
    // Start is called before the first frame update
    void Start()
    {
        textoCampo = GetComponentInChildren<Text>();
        anim = GetComponentInChildren<Animator>();
    }

    public void muestraMensaje(string msj, float tiempo)
    {
        textoCampo.text = msj;
        anim.Play("entrada");
        StartCoroutine(ocultaMsj(tiempo,anim));
    }

    private IEnumerator ocultaMsj(float tiempo,Animator animador)
    {
        yield return new WaitForSecondsRealtime(tiempo);
        anim.Play("salida");
    }
}
