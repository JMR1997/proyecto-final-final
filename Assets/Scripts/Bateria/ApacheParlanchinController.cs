﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ApacheParlanchinController : MonoBehaviour
{
    private Animator anim;
    public float tiempoMaxEntreAnims=60f;
    public float tiempoMinEntreAnims=15f;
    public float tiempoResToAnim;
    public List<string> listaNombresAnim;
    public List<string> listaFrases;
    private Text labelTextos;
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
        labelTextos = GetComponentInChildren<Text>();
        tiempoResToAnim = Random.Range(tiempoMinEntreAnims, tiempoMaxEntreAnims);
    }

    // Update is called once per frame
    void Update()
    {
        if (tiempoResToAnim > 0)
        {
            tiempoResToAnim -= Time.deltaTime;
        }
        else
        {
            int numAlea = Random.Range(0, listaNombresAnim.Count-1);
            labelTextos.text = listaFrases[Random.Range(0, listaFrases.Count - 1)];
            anim.Play(listaNombresAnim[numAlea]);
            tiempoResToAnim = Random.Range(tiempoMinEntreAnims, tiempoMaxEntreAnims);
        }
    }
}
