﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class generadorMapaBateria : MonoBehaviour
{
    public GameObject[] listaEstructuras;

    public void generaEstructura(Vector2 pos)
    {
        int posAlea = Random.Range(0,listaEstructuras.Length-1);
        Instantiate(listaEstructuras[posAlea],pos,Quaternion.identity);
    }
}
