﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class cuboBateriaController : MonoBehaviour
{
    public int puntosMax = 99;
    public int puntosMin = 1;
    private int puntosResActuales;
    public GameObject particulasExplotar;
    private TextMeshProUGUI textoNum;
    private SpriteRenderer sp;

    public Color colorMenos5;
    public Color colorMenos10;
    public Color colorMenos15;
    public Color colorMenos25;
    public Color colorMenos35;
    public Color colorMas40;

    private AudioManager am;
    // Start is called before the first frame update
    void Start()
    {
        GameManagerBateria gm = GameObject.FindObjectOfType<GameManagerBateria>();
        puntosMax = gm.getVida()*(1+(UnityEngine.Random.Range(0,40))/100);

        puntosResActuales = UnityEngine.Random.Range(puntosMin,puntosMax);
        textoNum = GetComponentInChildren<TextMeshProUGUI>();
        textoNum.text = puntosResActuales.ToString();
        sp = GetComponent<SpriteRenderer>();

        Color color = Color.white;
        if (puntosResActuales >= 5) color = colorMenos5;
        if (puntosResActuales >= 10) color = colorMenos10;
        if (puntosResActuales >= 15) color = colorMenos15;
        if (puntosResActuales >= 25) color = colorMenos25;
        if (puntosResActuales >= 35) color = colorMenos35;
        if (puntosResActuales >= 40) color = colorMas40;

        am = GameObject.FindObjectOfType<AudioManager>();
        sp.color = color;
    }

    public void bajarVida()
    {
        puntosResActuales--;
        textoNum.text = puntosResActuales.ToString();
        if (puntosResActuales % 3 == 0) { am.Play("puntos"); }

        if (puntosResActuales <= 0)
        {
            am.Play("puntos");
            //Elimina el objeto, explota
            Instantiate(particulasExplotar,transform.position,Quaternion.identity);
            Destroy(gameObject);
        }
    }

}

