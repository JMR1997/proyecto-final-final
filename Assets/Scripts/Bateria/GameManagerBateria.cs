﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.SceneManagement;

public class GameManagerBateria : MonoBehaviour
{
    private int vidaActualJugador = 5;
    public int score;
    private int monedas;
    private string claveScore = "Max Score QPB";

    public TextMeshProUGUI textoScore;
    public TextMeshProUGUI textoVida;

    public float tiempoEsperaSubirPuntos = 1.5f;
    private float tiempoResSubirPuntos;
   public  finJuegoBateriaScript menuFinDePartida;

    // Start is called before the first frame update
    void Start()
    {
        vidaActualJugador = 5;
        score = 0;
        monedas = 0;
        tiempoResSubirPuntos = tiempoEsperaSubirPuntos;
        GameObject.FindObjectOfType<AudioManager>().Play("bgMusic");
    }

    // Update is called once per frame
    void Update()
    {
        _FuncionesComunes.ComprobarCerrarAplicacion();

        if (tiempoResSubirPuntos > 0)
        {
            tiempoResSubirPuntos -= Time.deltaTime;
        }
        else
        {
            subirScore(1);
            tiempoResSubirPuntos = tiempoEsperaSubirPuntos;
        }
    }

    public void setVida(int vidaEstablecer)
    {
        if(vidaActualJugador < vidaEstablecer)
        {
            //animacion de cargar bateria
        }
        else
        {
            //animacion de bajar bateria
        }
        vidaActualJugador = vidaEstablecer;
        score++;
        textoVida.text = vidaEstablecer + "%";
        textoScore.text = String.Format("{0:0000}", score);
    }

    public int getVida()
    {
        return vidaActualJugador;
    }

    public void subirScore(int cantidad)
    {
        score += cantidad;
        textoScore.text = String.Format("{0:0000}", score);
        if (score > _FuncionesComunes.GetMaxScore(claveScore))
        {
            _FuncionesComunes.SaveScore(claveScore, score);
        }
    }

    public void terminarJuego()
    {
        //Aparece pantalla de fin del juego chula con sus sonidos y todo pro. Quizá con un objeto timeline?
        menuFinDePartida.gameObject.SetActive(true);
        //mapaDeJuego.SetActive(false);
        FindObjectOfType<AudioManager>().Stop("bgMusic");
        FindObjectOfType<jugadorBateriaController>().gameObject.SetActive(false);
    }

    public void recargarEscena()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void volverAtras()
    {
        _FuncionesComunes.CargarEscena("Menu");
    }
}
