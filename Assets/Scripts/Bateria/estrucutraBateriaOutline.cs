﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class estrucutraBateriaOutline : MonoBehaviour
{
    public int minObjVida = 1;
    public int maxObjVida = 2;
    public GameObject objetoVida;
    private void Start()
    {
        int cantidadAlea = UnityEngine.Random.Range(minObjVida,maxObjVida);
        for (int i = 0; i < cantidadAlea; i++)
        {
            generaObjetoVida();
        }
    }

    private void generaObjetoVida()
    {
        bool hayColision = true;
        int intento = 0;
        Vector2 spawnPoint = Vector2.zero;
        while (hayColision && intento<30){
            intento++;
            spawnPoint = generaPosAlea();
            Collider[] hitColliders = Physics.OverlapSphere(spawnPoint, 0.5f);//0.5 is purely chosen arbitrarly

            if (hitColliders.Length > 0) {
                hayColision = true;
            }
            else
            {
                hayColision = false;
            }
            
        }
        if (!hayColision) Instantiate(objetoVida,spawnPoint,Quaternion.identity);
    }


    private Vector2 generaPosAlea()
    {
        float x = UnityEngine.Random.Range(transform.position.x - 2.5f, transform.position.x + 2.5f);
        float y = UnityEngine.Random.Range(transform.position.y+0.5f, transform.position.y+4.5f);
        return new Vector2(x,y);
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        //Barra abajo
        Gizmos.DrawLine(new Vector2(transform.position.x - 2.8f, transform.position.y), new Vector2(transform.position.x + 2.8f, transform.position.y));
        //Barra arriba
        Gizmos.DrawLine(new Vector2(transform.position.x - 2.8f, transform.position.y + 5), new Vector2(transform.position.x + 2.8f, transform.position.y + 5));
        //Barras verticales
        Gizmos.DrawLine(new Vector2(transform.position.x - 2.8f, transform.position.y), new Vector2(transform.position.x - 2.8f, transform.position.y + 5));
        Gizmos.DrawLine(new Vector2(transform.position.x + 2.8f, transform.position.y), new Vector2(transform.position.x + 2.8f, transform.position.y + 5));

    }
}