﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class vidaBateria : MonoBehaviour
{
    private jugadorBateriaController jugador;
    public int cantMax = 9;
    public int cantMin = 1;
    public int cantidad;
    public GameObject particulasSubirVida;
    private TextMeshProUGUI textoValor;
    private void Start()
    {
        cantidad = Random.Range(cantMin,cantMax);
        //textoValor = GetComponentInChildren<TextMeshProUGUI>();
        //textoValor.text = cantidad.ToString();
        jugador = GameObject.FindObjectOfType<jugadorBateriaController>();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Destructor"))
        {
            Destroy(gameObject);
        }
        else if (collision.CompareTag("Player"))
        {
            //am.Play(nombreSonido);
            jugador.subirVida(cantidad);
            Instantiate(particulasSubirVida, transform.position, Quaternion.identity);
            Destroy(gameObject);
        }
    }
}
