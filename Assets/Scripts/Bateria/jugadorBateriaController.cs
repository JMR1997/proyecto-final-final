﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class jugadorBateriaController : MonoBehaviour
{
    Camera cam;

    public float velocidad = 6f;
    public float tiempoEntreColisiones = 0.3f;
    private int contColisionesSeguidas = 0;

    private float tiempoResCol;

    public int vidaInicial;
    private int vidaActual;

    private Vector2 posInicioRecorrido;
    public float velocidaSubida;
    private GameManagerBateria gm;

    public GameObject particulasPerderVida;
    public GameObject particulasExplotar;

    private Vector2 posUltEstructuraGenerada;
    private generadorMapaBateria generadorMapa;
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindObjectOfType<GameManagerBateria>();
        cam = Camera.main;
        tiempoResCol = 0;
        vidaActual = vidaInicial;
        //Se pone uan distancia mayor de 5 para que genera una estructura en el primer update
        posUltEstructuraGenerada = new Vector2(transform.position.x, transform.position.y-5);
        generadorMapa = GameObject.FindObjectOfType<generadorMapaBateria>();
    }

    private void Update()
    {
        //Mov horizontal 
        if (Input.GetMouseButton(0))
        {
            Vector2 posActualRaton = cam.ScreenToWorldPoint(Input.mousePosition);
            posActualRaton = new Vector2(posActualRaton.x,transform.position.y);
            transform.position = Vector2.MoveTowards(transform.position, posActualRaton, velocidad * Time.deltaTime);
        }

        if (Input.GetAxisRaw("Horizontal") != 0)
        {
            float movHorizontal = Input.GetAxisRaw("Horizontal");
            transform.position = new Vector2(
                                transform.position.x + movHorizontal * velocidad * Time.deltaTime,
                                transform.position.y);
        }

        //Recorta la posicon a una adecuada por si fuese necesario
        Vector3 pos = Camera.main.WorldToViewportPoint(transform.position);
        pos.x = Mathf.Clamp01(pos.x);
        transform.position = Camera.main.ViewportToWorldPoint(pos);

        //Comprueba si se ha desplazado mas de 5 bloques en la altura para generar una nueva estructura
        //Son 5 bloques porque las estructuras miden 5 de alto
        if (Math.Abs(posUltEstructuraGenerada.y - transform.position.y) >= 5)
        {
            generadorMapa.generaEstructura(new Vector2(0, generadorMapa.gameObject.transform.position.y));
            posUltEstructuraGenerada = transform.position;
        }

        //Cuenta atras para colisionar de nuevo. Llega más de 0 para controlar con el contador las veces que toca
        if (tiempoResCol >= -0.1)
        {
            tiempoResCol -= Time.deltaTime;
        }
        else//Si estaba colisionando ya paró de hacerlo, reseteamos el contador
        {
            contColisionesSeguidas = 0;
        }
        
    }

    private void FixedUpdate()
    {
        float porcentajeIncremento = 1 + ((float)vidaActual / 100f);
        //Mov vertical autommamtico
        transform.Translate(0, (velocidaSubida * porcentajeIncremento) * Time.deltaTime, 0);
    }

    //Aqui detecta que choca con un cuadrado
    private void OnTriggerStay2D(Collider2D collision)
    {
        cuboBateriaController cubo = collision.gameObject.GetComponent<cuboBateriaController>();
        if (cubo != null && tiempoResCol <= 0)
        {
            cubo.bajarVida();
            if (contColisionesSeguidas >= 6)
            {
                tiempoResCol = tiempoEntreColisiones / 5;
            }
            else if (contColisionesSeguidas >= 3)
            {
                tiempoResCol = tiempoEntreColisiones / 2;
            }
            else
            {
                tiempoResCol = tiempoEntreColisiones;
            }

            contColisionesSeguidas++;
            bajarVida();
        }
    }

    private void bajarVida()
    {
        //Baja vida, inicia animacion de bajar vida
        //Avisa a lgame manager para aumentar los puntos restantes
        Instantiate(particulasPerderVida,transform.position,Quaternion.identity);
        vidaActual--;
        gm.setVida(vidaActual);
        if (vidaActual <= 0)
        {
            Instantiate(particulasExplotar, transform.position, Quaternion.identity);
            gm.terminarJuego();
        }
    }

    public void subirVida(int cantidad)
    {
        vidaActual+=cantidad;
        if (vidaActual >= 100)
        {
            vidaActual = 100;
            gm.subirScore(10);
        }
        gm.setVida(vidaActual);
        GameObject.FindObjectOfType<AudioManager>().Play("recarga");
    }
}