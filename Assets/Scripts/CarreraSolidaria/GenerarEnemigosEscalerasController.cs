﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GenerarEnemigosEscalerasController : MonoBehaviour
{
    #region variables

    public GameObject enemigoPrefab;
    public float generatorTimer = 1.75f;

    #endregion

    //Funcion que utiliza el prefab del enemigo escalera y lo crea en una posicion determinada
    void crearEnemigo()
    {
        Instantiate(enemigoPrefab, transform.position, Quaternion.identity);
    }

    //Funcion que generar cada x tiempo un enemigo
    public void comenzarGenerador()
    {
        InvokeRepeating("crearEnemigo", 0f, generatorTimer);
    }

    /*
     * Funcion que para el generador de enemigos. Si el parametro pasado es verdadero, tambien los elimina del juego
     * @param bool
     */
    public void pararGenerador( bool clean=false)
    {
        CancelInvoke("crearEnemigo");
        if (clean)
        {
            Object[] allEnemigos = GameObject.FindGameObjectsWithTag("Enemigo");
            foreach (GameObject enemigo in allEnemigos)
            {
                Destroy(enemigo);
            }
        }
    }
}
