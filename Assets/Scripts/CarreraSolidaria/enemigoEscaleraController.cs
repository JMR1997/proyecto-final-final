﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class enemigoEscaleraController : MonoBehaviour
{
    public float velocidad = 2f;
    private Rigidbody2D rb2d; //Guarda al enemigo para añadirle velocidad posteriormente
    // Start is called before the first frame update
    void Start()
    {
        rb2d = GetComponent<Rigidbody2D>();
        rb2d.velocity = Vector2.left * velocidad;
    }

    //Sobreescribe el metodo OnTriggerEnter2D para comprobar si el enemigo colisona con el enemy destroyer
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "destroyer")
        {
            Destroy(gameObject);
        }
       
    }
}
