﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    #region variables
    /*********************************************************************************************
     * ELEMENTOS UI
     ***************************************************************************************************/ 
    public enum GameState { Parado, Jugando, Finalizado,PreparadoParaReinicio };
    [Range(0f, 0.20f)]
    public float velocidadParallax = 0.02f;
    public RawImage fondo;
    public RawImage plataforma;
    public GameObject uiParado;
    public GameObject uiScore;
    public GameObject uiGameOver;
    public GameObject corredor;
    public GameObject generadorEnemigos;
    public Text textoPuntos;
    public Text mejorPuntuacion;

    /******************************************************************************************
     * VARIABLES QUE CONTROLAN LA VELOCIDAD DEL JUEGO
     ******************************************************************************************/ 
    private float scaleTime = 6f;
    private float scaleInc = .15f;
  
    public GameState estadoDelJuego = GameState.Parado;

   
    
    private AudioManager musicaFondo;//Musica
    //Puntuaje
    private int puntos =0;
    private string claveScore = "Max Score CS";
    #endregion 

    // Start is called before the first frame update
    void Start()
    {

        musicaFondo= FindObjectOfType<AudioManager>();
        mejorPuntuacion.text = "Puntuación Maxima: " + _FuncionesComunes.GetMaxScore(claveScore).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        _FuncionesComunes.ComprobarCerrarAplicacion();
        EmpiezaJuego();
    }


    /*
    * Funcion que efecuta el efecto parallax sobre background y platform
    * @params void
    * 
    */
    void EfectoParallax()
    {
        float velocidadFinal = velocidadParallax * Time.deltaTime;//Time.deltaTime indica que lo multiplicamos por una velocidad estandar independientemente a la potencia de nuestro pc
        fondo.uvRect = new Rect(fondo.uvRect.x + velocidadFinal, fondo.uvRect.y, fondo.uvRect.width, fondo.uvRect.height);
        plataforma.uvRect = new Rect(plataforma.uvRect.x + velocidadFinal * 4, plataforma.uvRect.y, plataforma.uvRect.width, plataforma.uvRect.height);
    }
    /*
     * Funcion que comienza el juego
     * Si el estado del juego es parado y pulsas con el raton o con el dedo, cambia estadoJuego
     * Si el estado juego es Jugando invoca a la función efectoParallax
     */
    void EmpiezaJuego()
    {
        bool pulsaPantalla = Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Space);//Devuelve verdadero si el usuario ha pulsado la pantalla
        //Comienza el juego
        if (estadoDelJuego == GameState.Parado && pulsaPantalla)//Tambien vale para las pulsaciones de Android
        {
            estadoDelJuego = GameState.Jugando;

            uiParado.SetActive(false);//Desactiva la ui de inicio
            uiScore.SetActive(true);

            corredor.SendMessage("actualizarEstado", "coredorRun");//Llama a la funcion actualizarEstado de playerController
            corredor.SendMessage("polvoPlay");//Llama a la funcion polvoPlay de playerController

            generadorEnemigos.SendMessage("comenzarGenerador");//Llama a la función comenzarGenerador de GenerarEnemigosEscalerasController
            musicaFondo.Play("Fondo");
            InvokeRepeating("VelocidadDelJuego", scaleTime, scaleTime);//Cambia la velocidad del juego cada 6 segundos
        }
        else if (estadoDelJuego == GameState.Jugando)
        {
            EfectoParallax();
            
        }
        /*else if (estadoDelJuego == GameState.PreparadoParaReinicio)
        {
            if (pulsaPantalla)
            {
                ReiniciarJuego();
            }
        }*/
    }

    //Funcion que renicia el juego cargando la escena
    public void ReiniciarJuego()
    {
        ResetVelocidadDelJuego();
        _FuncionesComunes.CargarEscena("CarreraSolidaria");
    }

    //Función que incrementa la velocidad de juego cada 6 segundos
    void VelocidadDelJuego()
    {
        Time.timeScale += scaleInc;
    }

    //Metodo que rrestablece de nuevo la velocidad del juego
    public void ResetVelocidadDelJuego(float newTimeScale=1f)
    {
        CancelInvoke("velocidadDelJuego");
        Time.timeScale = newTimeScale;//Cantidad por defecto
    }
    //Metodo que incrementa los puntos
    public void IncrementarPuntos()
    {
        textoPuntos.text = (++puntos).ToString();
        if (puntos >= _FuncionesComunes.GetMaxScore(claveScore))
        {
            mejorPuntuacion.text= "Puntuación Maxima: " + puntos.ToString();
            _FuncionesComunes.SaveScore(claveScore,puntos);
        }
    }
    //Metodo que resetea los puntos
    public void ResetPuntos()
    {
        textoPuntos.text = "0";
    }
    //Metodo llamado por el boton cancelar
    public void btn_Cancelar()
    {
        _FuncionesComunes.CargarEscena("Menu");
    }
    //Muestra la interfaz de Game Over y detiene todos los elementos en pantalla
    public void GameOver()
    {
        estadoDelJuego = GameState.Finalizado;
        generadorEnemigos.SendMessage("pararGenerador", true);
        ResetVelocidadDelJuego(.5f);
        musicaFondo.Stop("Fondo");
        uiGameOver.SetActive(true);
        //ResetPuntos();
    }

}
