﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerContorler : MonoBehaviour
{
    #region variables

    private Animator animator;
    public GameObject juego;
    public GameObject generadorEnemigos;

    private string sonidoSalto="Jump";
    private string sonidoMuerte= "Damage";
    private string sonidoPuntos= "Points";

    public ParticleSystem polvo;

    private AudioManager audioManager;
    private float startY;

    #endregion
    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        audioManager = FindObjectOfType<AudioManager>();
        startY = transform.position.y;
    }

    // Update is called once per frame
    void Update()
    {
        saltar();
    }

    /*
     * Funcion que recibe como parametro el nombre de una animación y la activa al jugador
     * @param string
     */
    public void actualizarEstado(string estado=null)
    {
        if (estado != null)
        {
            animator.Play(estado);
        }
    }


    //Sobreescribe el metodo OnTriggerEnter2D para comprobar si el personaje choca contra los enemigos
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Enemigo")
        {
            gameOver();
        }else if( collision.gameObject.tag == "Puntos")
        {
            juego.SendMessage("IncrementarPuntos");
            audioManager.Play(sonidoPuntos);
        }

    }

    //Funcion que cambia a la animación de salto del personaje cuando se pulsa el ratón o la pantalla
    private void saltar()
    {
        bool estaEnSuelo = transform.position.y == startY;
        bool gamePlaying = juego.GetComponent<GameController>().estadoDelJuego == GameController.GameState.Jugando;
        if (gamePlaying && (Input.GetMouseButtonDown(0) || Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.Space)) && estaEnSuelo)
        {
            actualizarEstado("corredorSalto");
            audioManager.Play(sonidoSalto);
            
            
        }
    }

    //Funcion que activa la animación de muerte y su sonido. Además envia una señal al GeneradorDeEnemigos para que acabe su ejecucción
    private void gameOver()
    {
        actualizarEstado("corredorMuerte");
        polvoStop();
        audioManager.Play(sonidoMuerte);
        juego.GetComponent<GameController>().GameOver();
    }
    //Funcion llamada en la animacion de la muerte que avisa al gamecontroller que el juego ya se puede reiniciar
    private void JuegoPreparadoParaReiniciar()
    {
        juego.GetComponent<GameController>().estadoDelJuego = GameController.GameState.PreparadoParaReinicio;
    }



    //Estas dos funciones controlan las particulas de polvo y han sido creadas para manejarlas desde la animación de correr
    private void polvoStop()
    {
        polvo.Stop();
    }
    private void polvoPlay()
    {
        polvo.Play();
    }
}
