﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class _FuncionesComunes : MonoBehaviour
{
    private static float tiempoMaxEntreToquesParaSalir = 1.5f;
    private static float tiempoRest;
    private static int numToques;


    //Este metodo se llama siempre en el update asi que es como un update
    public static void ComprobarCerrarAplicacion()
    {
        if (tiempoRest > 0) { tiempoRest -= Time.deltaTime; }
        else { numToques = 0; }

        if (Input.GetKeyUp(KeyCode.Escape))
        {
            numToques++;
            tiempoRest = tiempoMaxEntreToquesParaSalir;
            #region android
            //if (Application.platform == RuntimePlatform.Android)
            //{
            if (numToques >= 2)
            {
                numToques = 0;
                tiempoRest = 0;
                if (SceneManager.GetActiveScene().name != "Menu")
                {
                   CargarEscena("Menu");
                }
                else
                {
                    if (Application.platform == RuntimePlatform.Android) { AndroidJavaObject activity = new AndroidJavaObject("com.unity2d.player.UnityPlayer").GetStatic<AndroidJavaObject>("currentAcitvity"); }
                    else { Application.Quit(); }
                }
            }
            else
            {
                MessageGenerator ms = FindObjectOfType<MessageGenerator>();
                if(ms != null) { ms.muestraMensaje("Vuelva a pulsar para salir",tiempoMaxEntreToquesParaSalir); }
            }
            //}
            #endregion
            /*else
            {
                Application.Quit();
            }*/
        }
    }
    public static int GetMaxScore(string clave)
    {
        return PlayerPrefs.GetInt(clave, 0);
    }
    public static void SaveScore(string clave,int puntuajeActual)
    {
        PlayerPrefs.SetInt(clave, puntuajeActual);
    }
    public static void CargarEscena(string escena)
    {
        Time.timeScale = 1f; //Poner tiempo a velocidad normal porqeu jaime es una perosna muy lista que no sabe haacer juegos. Atentamente, Jaime <3
        SceneManager.LoadScene(escena);
    }

}
