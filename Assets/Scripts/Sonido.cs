﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

[System.Serializable]
public class Sonido
{
    public string nombre;
    public AudioClip clip;

    public bool loop = false;

    [Range(0f,1f)]
    public float volumen = 3f;
    [Range(0.1f, 3f)]
    public float pitch;
    [HideInInspector]
    public AudioSource source;

}
